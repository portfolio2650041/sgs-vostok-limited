const app = Vue.createApp({
    data() {
      return {
        data: [
            { city: "Город 1", workshop: "Цех 1", employee: "Сотрудник 1", shift: "Дневная" },
            { city: "Город 1", workshop: "Цех 1", employee: "Сотрудник 2", shift: "Ночная" },
            { city: "Город 1", workshop: "Цех 2", employee: "Сотрудник 3", shift: "Дневная" },
            { city: "Город 2", workshop: "Цех 1", employee: "Сотрудник 4", shift: "Дневная" },
            { city: "Город 2", workshop: "Цех 1", employee: "Сотрудник 5", shift: "Ночная" },
            { city: "Город 2", workshop: "Цех 2", employee: "Сотрудник 6", shift: "Дневная" },
            { city: "Город 3", workshop: "Цех 1", employee: "Сотрудник 7", shift: "Дневная" },
            { city: "Город 3", workshop: "Цех 1", employee: "Сотрудник 8", shift: "Ночная" },
            { city: "Город 3", workshop: "Цех 2", employee: "Сотрудник 9", shift: "Дневная" },
            { city: "Город 4", workshop: "Цех 1", employee: "Сотрудник 10", shift: "Дневная" },
            { city: "Город 4", workshop: "Цех 1", employee: "Сотрудник 11", shift: "Ночная" },
            { city: "Город 4", workshop: "Цех 2", employee: "Сотрудник 12", shift: "Дневная" },
            { city: "Город 5", workshop: "Цех 1", employee: "Сотрудник 13", shift: "Дневная" },
            { city: "Город 5", workshop: "Цех 1", employee: "Сотрудник 14", shift: "Ночная" },
            { city: "Город 5", workshop: "Цех 2", employee: "Сотрудник 15", shift: "Дневная" },
            { city: "Город 6", workshop: "Цех 1", employee: "Сотрудник 16", shift: "Дневная" },
            { city: "Город 6", workshop: "Цех 1", employee: "Сотрудник 17", shift: "Ночная" },
            { city: "Город 6", workshop: "Цех 2", employee: "Сотрудник 18", shift: "Дневная" },
            { city: "Город 7", workshop: "Цех 1", employee: "Сотрудник 19", shift: "Дневная" },
            { city: "Город 7", workshop: "Цех 1", employee: "Сотрудник 20", shift: "Ночная" }
        ],
        selectedCity: '',
        selectedWorkshop: '',
        selectedEmployee: ''
      };
    },
    computed: {
      cities() {
        return [...new Set(this.data.map(item => item.city))];
      },
      workshops() {
        return [...new Set(this.data.filter(item => item.city === this.selectedCity).map(item => item.workshop))];
      },
      employees() {
        return [...new Set(this.data.filter(item => item.city === this.selectedCity && item.workshop === this.selectedWorkshop).map(item => item.employee))];
      },
      filteredData() {
        return this.data.filter(item =>
          (!this.selectedCity || item.city === this.selectedCity) &&
          (!this.selectedWorkshop || item.workshop === this.selectedWorkshop) &&
          (!this.selectedEmployee || item.employee === this.selectedEmployee)
        );
      }
    },
  mounted() {
     const selectedCity = this.getCookie('selectedCity');
    if (selectedCity) {
      this.selectedCity = selectedCity;
    }
    const selectedWorkshop = this.getCookie('selectedWorkshop');
    if (selectedWorkshop) {
      this.selectedWorkshop = selectedWorkshop;
    }
    const selectedEmployee = this.getCookie('selectedEmployee');
    if (selectedEmployee) {
      this.selectedEmployee = selectedEmployee;
    }
  },
    methods: {
        saveToCookie(key) {
          this.setCookie(key, this[key], 30);
        },
        setCookie(name, value, days) {
          const date = new Date();
          date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
          const expires = "expires=" + date.toUTCString();
          document.cookie = name + "=" + value + "; " + expires;
        },
        getCookie(name) {
          const cookieName = name + "=";
          const cookies = document.cookie.split(';');
          for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i];
            while (cookie.charAt(0) === ' ') {
              cookie = cookie.substring(1);
            }
            if (cookie.indexOf(cookieName) === 0) {
              return cookie.substring(cookieName.length, cookie.length);
            }
          }
          return "";
        },
      }
  });
  
  app.component('app', {
    template: '#app',
  });
  
  app.mount('#app');
  
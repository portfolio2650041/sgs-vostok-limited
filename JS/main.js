const data = [
    { city: "Город 1", workshop: "Цех 1", employee: "Сотрудник 1", shift: "Дневная" },
    { city: "Город 1", workshop: "Цех 1", employee: "Сотрудник 2", shift: "Ночная" },
    { city: "Город 1", workshop: "Цех 2", employee: "Сотрудник 3", shift: "Дневная" },
    { city: "Город 2", workshop: "Цех 1", employee: "Сотрудник 4", shift: "Дневная" },
    { city: "Город 2", workshop: "Цех 1", employee: "Сотрудник 5", shift: "Ночная" },
    { city: "Город 2", workshop: "Цех 2", employee: "Сотрудник 6", shift: "Дневная" },
    { city: "Город 3", workshop: "Цех 1", employee: "Сотрудник 7", shift: "Дневная" },
    { city: "Город 3", workshop: "Цех 1", employee: "Сотрудник 8", shift: "Ночная" },
    { city: "Город 3", workshop: "Цех 2", employee: "Сотрудник 9", shift: "Дневная" },
    { city: "Город 4", workshop: "Цех 1", employee: "Сотрудник 10", shift: "Дневная" },
    { city: "Город 4", workshop: "Цех 1", employee: "Сотрудник 11", shift: "Ночная" },
    { city: "Город 4", workshop: "Цех 2", employee: "Сотрудник 12", shift: "Дневная" },
    { city: "Город 5", workshop: "Цех 1", employee: "Сотрудник 13", shift: "Дневная" },
    { city: "Город 5", workshop: "Цех 1", employee: "Сотрудник 14", shift: "Ночная" },
    { city: "Город 5", workshop: "Цех 2", employee: "Сотрудник 15", shift: "Дневная" },
    { city: "Город 6", workshop: "Цех 1", employee: "Сотрудник 16", shift: "Дневная" },
    { city: "Город 6", workshop: "Цех 1", employee: "Сотрудник 17", shift: "Ночная" },
    { city: "Город 6", workshop: "Цех 2", employee: "Сотрудник 18", shift: "Дневная" },
    { city: "Город 7", workshop: "Цех 1", employee: "Сотрудник 19", shift: "Дневная" },
    { city: "Город 7", workshop: "Цех 1", employee: "Сотрудник 20", shift: "Ночная" }
  ];

  const citySelect = document.getElementById("citySelect");
  const workshopSelect = document.getElementById("workshopSelect");
  const employeeSelect = document.getElementById("employeeSelect");
  const resultsDiv = document.getElementById("results");
  
  populateSelect(citySelect, data, "city");
  populateSelect(workshopSelect, data, "workshop");
  populateSelect(employeeSelect, data, "employee");
  
  citySelect.addEventListener("change", () => {
    updateSelects("city");
  });
  
  workshopSelect.addEventListener("change", () => {
    updateSelects("workshop");
  });
  
  employeeSelect.addEventListener("change", () => {
    updateSelects("employee");
  });
  
  function populateSelect(select, data, key) {
    const uniqueValues = [...new Set(data.map(item => item[key]))];
    uniqueValues.forEach(value => {
      const option = document.createElement("option");
      option.value = value;
      option.textContent = value;
      select.appendChild(option);
    });
  }
  
  function updateSelects(selectName) {
    const selectMap = {
      city: { current: citySelect, others: [{ s: workshopSelect, k: "workshop" }, { s: employeeSelect, k: "employee" }] },
      workshop: { current: workshopSelect, others: [{ s: citySelect, k: "city" }, { s: employeeSelect, k: "employee" }] },
      employee: { current: employeeSelect, others: [{ s: citySelect, k: "city" }, { s: workshopSelect, k: "workshop" }] }
    };
  
    const { current, others } = selectMap[selectName];
  
    const selectedFor = current.value;
  
    for (const selector of others) {
      selector.s.innerHTML = '<option value="">Выберите...</option>';
      const selectorFor = [...new Set(data.filter(item => item[selectName] === selectedFor))];
      populateSelect(selector.s, selectorFor, selector.k);
    }
  
    const selectedCity = citySelect.value;
    const selectedWorkshop = workshopSelect.value;
    const selectedEmployee = employeeSelect.value;
  
    const filteredData = data.filter(item =>
      (!selectedCity || item.city === selectedCity) &&
      (!selectedWorkshop || item.workshop === selectedWorkshop) &&
      (!selectedEmployee || item.employee === selectedEmployee)
    );
  
    resultsDiv.innerHTML = ""; 
  
    if (filteredData.length > 0) {
      filteredData.forEach(item => {
        const resultItem = document.createElement("div");
        resultItem.textContent = `Город: ${item.city}, Цех: ${item.workshop}, Сотрудник: ${item.employee}, Бригада: ${item.shift}`;
        resultsDiv.appendChild(resultItem);
      });
    } else {
      resultsDiv.textContent = "Нет результатов";
    }
  }